
def makeNgxModule(moduleName):
    return
    """
    ngx_module_t ngx_http_{moduleName}_module = {
        NGX_MODULE_V1,
        &ngx_http_{moduleName}_module_ctx, //module context
        NGX_HTTP_MODULE,                   //module type
        NULL,                              //init master
        NULL,                              //init module,
        NULL,                              //init process,
        NULL,                              //init thread,
        NULL,                              //exit thread,
        NULL,                              //exit process,
        NULL,                              //exit master,
        NGX_MODULE_V1_PADDING
    };
    """.format(moduleName=moduleName)

    
def makeNgxHandler(moduleName):
    return """
    static ngx_int_t
    ngx_http_{moduleName}_handler(ngx_http_request_t* req){
        ngx_http_{moduleName}_loc_conf_t *
           {moduleName}_config = ngx_http_get_module_loc_conf(req, ngx_http_{moduleName}_module);
    
    }
    """.format(moduleName=moduleName)


def makeNgxHeadOut(contentType):
    return """
    r->headers_out.status = NGX_HTTP_OK;
    r->headers_out.content_length_n = 100;
    r->headers_out.content_type.len = sizeof("{contentType}") -1;
    r->headers_out.content_type.data = (u_char *)"{contentType}";
    ngx_http_send_header(r);
    """.format(contentType)

def makeNgxHeadSsend():
    return """
    r->headers_out.content_encding = ngx_list_push(&r->headers_out.headers);
    if(r->headers_out.content_encoding == NULL){
        return NGX_ERROR;
    }

    r->headers_out.content_encoding->hash = 1;
    r->headers_out.content_encoding->key.len = sizeof("Content-Encoding")-1;
    r->headers_out.content_encoding->key.data = (u_char*)"Content-Encoding;

    
    """

