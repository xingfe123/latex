(TeX-add-style-hook
 "LispInSmallPieces"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "bottom=0.5in") ("fontenc" "T1")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "tothereader"
    "thebasicsofinterpretation"
    "lisp,1,2"
    "escap_return:continuations"
    "assignmentandsideffects"
    "denotationalsemantics"
    "fastinterpretation"
    "compilation"
    "evaluation_reflection"
    "chapter10"
    "chapter11"
    "book"
    "bk10"
    "geometry"
    "listings"
    "hyperref"
    "booktabs"
    "makeidx"
    "tikz"
    "fontenc"
    "exercise")
   (TeX-add-symbols
    "ie")
   (LaTeX-add-environments
    '("problem" LaTeX-env-args ["argument"] 1)))
 :latex)

