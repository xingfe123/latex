current=adaboost.pdf
#bayesian_non.pdf
CLEANFILES=*.aux *.log *.out *.idx *.nav *.snm *.toc
build/%.pdf: %.tex
	pdflatex $<
all:
	for file in $$(ls *.tex);do pdflatex $$file;done	

build/bayesian_non.pdf:
	xelatex bayesian_non.tex
build/pattern.pdf:pattern.tex
	xelatex pattern.tex
build/adaboost.pdf:adaboost.tex
	xelatex adaboost.tex
%.pdf: %.tex
	latex $<
all:
	for file in $$(ls *.tex);do pdflatex $$file;done	

test: $(current)
	mv  $(current) ~/windows/tex
cleandist:
	-rm $(CLEANFILES)
hello.o:helloKernel.c
	gcc  -D__KERNEL__ -DMODULE -I/usr/src/linux/include   -Wall -c -o $@ $<
