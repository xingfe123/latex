\chapter{Escape \& Return: Continuations}

Every computation has the goal of returning a value to a certain entity that we call a \emph{continuation}. This  chapter explains that idea and its historic roots. We'll also define a new interpreter, one that makes continuations explicit. In doing so, we'll present various implementations in Lisp and Scheme and we'll go into greater depth about the programming style known as ``Continuation Passing style.'' Lisp is distinctive among programming languages because of its elaborate forms for manipulating execution control. In some respects, that richness in Lisp will make this chapter seem like an enormous catalogue [moz87] where you'll probably feel like you've seen a thousand and three control forms one by one. In other respects, however, we'll keep a veil over continuations, at least over how they are physically carried out. Our new interpreter will use objects to show the relatives of continuations and its control blocks in the \emph{evaluation stack}.

The interpreters that we built in earlier chapters took an expression and an environment in order to determine the value of the expression. However, those interpreters were not capable of defining computations that included \emph{escapes}, useful control structures that involve getting out of one context in order to get into another, more preferable one. In conventional programming, we use escapes principally to master the behavior of programs in case of unexpected errors, or to program by exceptions when we define a general behavior where the occurrence of a particular event interrupts the  current calculation and sends it back to an appropriate place.

The historic roots of escapes go all way back to Lisp 1.5 to the form prog. Though that form is now obsolete, it was originally introduced in the vain hope of attracting Algol users to Lisp since it was widely believed that they knew how to program only with goto. Instead, it seems that the form distracted otherwise healthy Lisp programmers away from the precepts\footnote{As an example, consider the stylistic differences between the first and third editions of [wh88].} of tail-recursive functional programming. Nevertheless, as a form, prog merits attention because it embodies several interesting traits. Here, for example, is factorial written with prog.

\begin{lstlisting}
(defun fact (n)
  (prog (r)
        (setq r 1)
        loop (cond ((= n 1) (return r))
                   (setq r (* n r))
                   (setq n (- n 1))
                   (go loop))))
\end{lstlisting}

The special form prog first declares the local variables that it uses (here, only the variable r). The expressions that follow are instructions (represented as lists) or labels (represented by symbols). The instructions are evaluated sequentially like in progn; and normally the value of a prog form is nil. Multiple special instructions can be used in one prog. Unconditional jumps are specified by go (with a label which is not computed) whereas return imposed the final value of the prog form. There was one restriction in Lisp 15: go forms and return forms could appear only in the first level of a prog or in a cond at the first level.

\begin{lstlisting}
(defun fact2 (n)
  (prog (r)
        (seq r 1)
        loop (setq r (* (cond ((= n 1) (return r))
                              ('else n))
                        r))
        (setq n ( - n 1))
        (go loop)))
\end{lstlisting}

If we reduce the forms return and prog to nothing more than their control effects, we see clearly that they handle the calling point (and the return point) of the form prog exactly like in the case of a functional application where the invoked function returns its value precisely to the place where the invoked function returns its value precisely to the place where it was called. In some sense, then, we can say that prog binds return to the precise point that it must come back to with a value. Escape, then, consists of not knowing the place from which we take off while specifying the place where we want to arrive. Additionally, we hope that such a \emph{jump} will be implemented efficiently: escape then becomes a programming method with its own disciples. You can see it in the following example of searching for a symbol in a binary tree. A naive programming style for this algorithm in scheme would look like this:
\begin{lstlisting}
(define (find-symbol id tree)
  (if (pair? tree)
      (or (find-symbol id (car tree))
          (find-symbol id (cdr tree)))
      (eq? tree id)))
\end{lstlisting}

Let's assume that we're looking for the symbol foo in the expression ((( a. b))) .(foo . c)) . (d . e)). Since the search takes place from left to right and depth-first, once the symbol has been found, then the value \#t must be passed across several embedded or forms before it becomes the final value (the whole point of the search, after all). The following shows the details of the preceding calculation in equivalent forms.
\begin{lstlisting}
(find-symbol 'foo '(((a . b) . (foo. c)) . (d . e)))  
=(or (find-symbol 'foo '((a . b) . (foo. c))) 
     (find-symbol 'foo '(d . e)))
=(or (or (find-symbol 'foo '(a . b))
         (find-symbol 'foo '(foo. c)))
     (find-symbol 'foo '(d . e)))
= (or (or (or (find-symbol 'foo 'a)
              (find-symbol 'foo 'b))
          (find-symbol 'foo '(foo. c)))
      (find-symbol 'foo '(d . e)))
= (or (or (find-symbol 'foo 'b)
          (find-symbol 'foo '(foo. c)))
      (find-symbol 'foo '(d . e)))
= (or (find-symbol 'foo '(foo. c))
      (find-symbol 'foo '(d . e)))
= (or (or (find-symbol 'foo 'foo)
          (find-symbol 'foo 'c))
      (find-symbol 'foo '(d . e)))
= (or (or #t
          (find-symbol 'foo 'c))
      (find-symbol 'foo '(d . e)))
= (or #t
     (find-symbol 'foo '(d . e))
= #t
\end{lstlisting}

An efficient escape seems appropriate there. As soon as the symbol that is being searched for has been found, an efficient escape has to support the return of the very same value without considering any remaining branches of the search tree.

Another example comes from programming by exception where repetitive treatment is applied over and over again until an exceptional situation is detercted in which case an escape is carried out in order to escape from the repetitive treatment that would otherwise continue more or less perpetually. We'll see an example of this later with the function better-map. [see p. 80]

If we want to characterize the entity corresponding to a calling point better, we note that a calculation specifies not only the expression to compute and the environment of variables in which to compute it, but also where we must return the value obtained. This ``where'' is known as a \emph{continuation}. It represents all that remains to compute.

Every computation has a continuation. For example, in the expression (+ 3 (* 2 4)), the continuation of the subexpression (* 2 4) is the computation of an addition where the first argument is 3 and the second is expected. At this point, theory comes to our rescue: this continuation can be represented in a form that's easier to see -- as a function. A continuation represents a computation, and it does't take place unless we get a value for it. This protocol strongly resembles the one for a function and is thus in that guise that continuations will be represented. For the preceding example, the continuation of the subexpression (* 2 4) will thus be equivalent to (lambda (x) (+ 3 x)), thus faithfully underlining the fact that the calculation waits for the second argument in the addition.

Another representation also exists; it specifies continuations by contexts inspired from $\lambda$-calculus. In that representation, we would denote the preceding continuation as (+ 3 []), where [] stands for the place where the value is expected.

Truly everything has a continuation. The evaluation of the condition of an alternative is carried out by means of a continuation that exploits the value that will be returned in order to decide whether to take the true or false branch of the alternative. In the expression (if (foo) 1 2 ), the continuation of the functional application (foo) is thus (lambda (x) (if x 1 2)) or (if [] 1 2).

Escape, programming by exception, and so forth are merely particular forms for manipulating continuations. With that idea in mind, now we'll detail the diverse forms and great variety of continuations observed over the past twenty years or so.

\section{Forms for Handling Continuations}

Capturing a continuation make it possible to handle the control thread in a program. The form prog already makes it possible to do that, but carries with it other effects, like those of a let for its local variables. Paring down prog so that it concern only control was the first goal of the forms catch and throw. 

\subsection{The Pair catch/throw}
The special form catch has the following syntax:
\begin{lstlisting}
  (catch label forms ...)
\end{lstlisting}

The first argument, label, is evaluated, and its value is associated with the current continuation. This facts makes us suppose that there must be a new space, the \emph{dynamic escape space}, binding values and continuations. If we can't think of labels which are not identifiers, we actually can't talk anymore about a name space; this, however, really is one, if we accept the fact that every value can be a valid label for that space. Yet that condition poses the problem of equality within this space: how can we recognize that one value names what another has associated with a continuation?

The other forms make up the body of the form catch and are themselves evaluated sequentially, like in a progn or in a begin. If nothing happens, the value of the form catch is that of the last of the forms in its body. However, what can intervene is the evaluation of a throw.

The form \emph{throw} has the following syntax.
\begin{lstlisting}
throw label form
\end{lstlisting}

The first argument is evaluated and must lead to a value that a dynamically embedding catch has associated with a continuation. If such is the case, then the evaluation of the body of this catch form will be interrupted, and the value of the form will become the value of the entire catch form. 

Let's go back to our example of searching for a symbol in a binary tree, and let's put into it the forms catch and throw. The variation that you are about to see has factored out the search process in order not to transmit the variable id pointlessly, since it is lexically visible in the entire search. We will thus use a local function to do that. 

\begin{lstlisting}
(define (find-symbol id tree)
  (define (find tree)
    (if (pair? tree)
        (or (find (first tree))
            (find (rest tree)))
        (if (eq? tree id) (throws 'find #t) #f)))
  (catch 'find (find tree)))
\end{lstlisting}

As its name indicates, catch traps the value of throw sends it. An escape is consequently a direct transmission of the value associated with a control manipulation. In other words, the form catch is a binding form that associates the current continuation with a label. The form throw actually makes the reference to this binding. That form also changes the thread of control as well: throw does not really have a value because it returns no result by itself, but it organizes things in such a way that catch can return a value. Here, the form catch captures the continuation of the call to find-symbol, while throw accomplishes the direct return to the caller of find-symbol.

The dynamic escape space can be characterized by a chart listing its various properties.
\begin{table}
  \centering
  \begin{tabular}{l l }
    Reference & (throw label ...) \\
    Value & no. it's a second class continuation \\
    Modification & no \\
    Extension& (catch label ...)\\
    Definition & no \\
  \end{tabular}
\end{table}

As we said earlier, catch is not really a function, but a special form that successively evaluates its first argument (the label), binds its own continuation to this latter value in the dynamic escape environment, and then begins the evaluation of its other arguments. Not all of them will necessarily be evaluated. When catch returns a value or when we escape from it, catch dissociates the label from the continuation. 

The effect of throw can be accomplished either by a function or by a special form. When it is a special form, as it is in Common Lisp, it calculates the label, it verifies the existence of an associated catch, then it evaluates the value to transmit, and finally it jumps. When throw is a function, it does things in a different order: it first calculates the throw arguments, then verifies the existence of an associated catch, and then it jumps. 

The semantic differences clearly slow how unreliably a text might describe the behavior of these control forms. A lot of questions remain unanswered. What happens, for example, when there is no associated catch? Which equality is used to compare labels? What does this expression do $(throw \alpha (throw \beta \pi)$? We'll find answers to those kinds of questions a little later. 

\subsection{The Pair block/return-from}

The escapes that catch and throw perform are dynamic. When throw requests an escape, it must verify during execution whether an associated catch exists, and it must also determine which continuation it refers to. In terms of implementation, there is a non-negligible cost here that might hope to reduce by means of lexical escapes, as they are known in the terminology of Common Lisp. The special forms block and return-from superficially resemble catch and throw. 

Here's the syntax of the special form block:
\begin{lstlisting}
  (block label forms)
\end{lstlisting}
The first argument is not evaluated and must be a name suitable for the escape:
an identifier. The form block binds the current continuation to the name label within the lexical escape environment. The body of block is evaluated then in implicit progn with the value of this progn becoming the value of block. This sequential evaluation can be interrupted by return-from.

Here's the syntax of the special form return-from:
\begin{lstlisting}
  (return-from label form)
\end{lstlisting}
That first argumnet is not evaluated and must mention the name of an
escape that is lexically visible; that is, a return-from form can appear only in the body of an associated block, just as a variable can apear only in the body of an associated lambda form. When a return-from from is evaluated, it makes the associated block return the value of form.

Lexical escapes generate a new name space, and we'll summarize its properties in the following chart.
\begin{table}
  \centering
  \begin{tabular}{c l }
    Reference& (return-from label ...)\\
    Value & no because it's a second class continuation\\
    Modification & no \\
    Extension & (block label ...)\\
    Definition & no \\
  \end{tabular}
\end{table}
Let's look again at the preceding example, this time, write\footnote{Warning to Scheme-users: define is translated internally into a letrec because (let () (define ...) ...) is not valid in Scheme because of the ().}it like this:
\begin{listing}
(define (find-symbol id tree)
  (block find
		 (letrec ((find (lambda (tree)
						  (if (pair? tree)
							  (or (find (car tree))
								  (find (cdr tree)))
							  (if (eq? id tree) (return-from find #t)
								  #f)))))
		   (find tree))))
\end{listing}
Notice that this is not an ordinary translation of the previous example where ``catch 'find'' simply turns into ``block find'' nor likewise for throw. Without the miguation of ``block find'', the form return-from would not have been bound to the appropriate block.

The generated code corresponding to that form is highly efficient. In gross, general terms, block stores the height of the execution stack under the name of the escape find. The form return-from consists solely of putting #t where a value is expected (for example, in a register) and re-adjusting the stack pointer to the height that block associated with find. This represents only a few instructions, in contrast to thw dynamic behavior of catch. catch would work through the entire list of valid escapes, little by little, until it found one with the right label. You can see that difference clearly if you consider this simulation of catch\footnote{3} by block:
\begin{listing}
  (define *active-catcher* '())
(define-syntax throw
  (syntax-rules ()
	((throw tag value)
	 (let* ((label tag)
			(escape (assv label *active-catcher*)))
	   (if (pair? escape)
		   ((cdr escape) value)
		   (wrong "No associated catch to" label))))))
(define-syntax catch
  (syntax-rules ()
	((catch tag . body)
	 (let* ((saved-catches *active-catcher*)
			(result (block label
						   (set! *active-catcher*
							 (cons (cons tag
										 (lambda (x)
										   (return-from label x)))
								   *active-catcher*))
						   . body)))
	   (set! *active-catcher* saved-catches)
	   result))))
\end{listing}

In that simulation, the cost of the pair catch/throw is practically entirely concentrated in the call to assv\footnote{In passing, you see that the labels are compared by eqv?} in the expansion of throw. But first, let's explain how th simulation works. A global variable(here, named *active-catchers*) keeps track of all the active catch forms, that is, those whose execution has not yet been completed. That variable is updated at the exit from catch and consequently at the exit from throw. The value of *active-catchers* is an A-list, where the keys are the labels of catch and the values are the associated continuations. This A-list embodies the dynamic escape environment for which catch was the binding form and for which throw was the referencing form, as you can easuly see in that simulation code.

\subsection{Escapes with a Dynamic Extent}
\label{sec:escapes-with-dynamic}

That simulation, however, is imperfect in the sense that it prohibits simulataneous uses of block; doing so would perturb the value of the variable *active-catchers*. The art of simulation or syntactic extension of a dialect is difficult one, as [Fel90, Bak92c] observe, because frequently adding new traits requires a complicated architecture that prohibits direct access to resources mobilized for the simulation or extension. Later, we'll show you an authentic simulation of catch by block, but it will require yet another linguistic means: unwind-protect\footnote{Another solution would be to redefine the new forms block and return-from, with the help of the old ones, so that they would be compatible with catch and throw. That solution is difficult to implement directly, but it can be done by adding a new level of interpretation.}

Like all entities in Lisp, continuations have a certain extent. In the simulation of catch by block, you saw that the extent of a continuation caught by catch is limited to the duration of the calculation in the body of the catch in question. We refer to this extent as dynamic. When we use that term, we also thinking of dynamic binding since it insures only that the value of a dynamic variable lasts as long as the evaluation of the body of the corresponding binding form. We could take advantage of this property to offer a new definition of catch and throw in term of block and return-from; this time, the list of catchers that can be activated is easily maintained in the dynamic variable *active-catchers*. This program reconciles block and catch so that they can be used simultaneously now. 
\begin{listing}
(define-syntax throw
  (syntax-rules ()
	((throw tag value)
	 (let* ((label tag)
			(escape (assv label (dynamic *active-catcher*))))
	   (if (pair? escape)
		   ((cdr escape) value)
		   (wrong "No associated catch to" label))))))

(define-syntax catch
  (syntax-rules ()
	((catch tag . body)
	 (block label
			(dynamic-let ((*active-catcher*
						   (cons (cons tag (lambda (x)
											 (return-from label x)))
								 (dynamic *active-catcher*))))
						 . body )))))
\end{listing}
The extent of an escape caught by block is dynamic in Common Lisp, so the escape can be used only during the calculation of the body of the block. Likewise, the extent of an escape caught by catch is also dynamic in Common Lisp, so again, the escape can be used only during the calculation of the body of the catch. This fact pose an interesting problem with block, a problem that does not occur with catch: if throw and return-from make it possible to abbreviate computations, then some computation must exist to be escaped from. Consider the following program.
\begin{listing}
((block foo (lambda (x)
			  (return-from foo x)))
 33)
\end{listing}
The value of the first term is an escape to foo, but this escape is obsolete when it is applied, and in consequence, we get an execution error. When the closure is created, it closes the lexical escape environment, especially the binding of foo. That closure is then returned as the value of the form block, but that return occurs when we exit from block, and consequently, it is out of the question to exit yet again, since that has already happened. When that closure is applied, we verify whether the continuation associated with foo is still valid, and we jump if that is the case. The following example shows that closures around lexical escapes do not mix bindings. 

\begin{listing}
(block foo
  (let ((f1 (lambda (x) (return-from foo x))))
     (* 2 (block foo (f1 1 )))))		 
\end{listing}
Compare those result with what we would get by replacing ``block foo'' by ``catch 'foo'' like this:
\begin{listing}
  (catch 'foo
  (let ((f1 (lambda (x) (throw 'foo x))))
	(* 2 (catch 'foo
		   (f1 1)))))
\end{listing}
The catcher invoked by the funciton f1 is the more recent one, having bound the label foo; the result of catch is consequently mulitplied by 2 and returns 2 finally. 

\subsection{Comparing catch and block}
\label{sec:comp-catch-block}

The forms catch and block have many points of comparison. The continuations that they capture have a dynamic extent: they last only as long as an evaluation. In contrast, return-from can refer an indefinitely long time to a continuation, whereas throw is more limited. In terms of efficiency, block is better in most cases because it never has to verify during a return-from whether the corresponding block exists, since that existence is guaranteed by the syntax. However, it is necessary to verify that the escape is not obsolete, though that fact is often syntactically visible. You can see a parsllel betwen dynamic and lexical escapes, on one side, and dynamic and lexical variables, on the other: many problems are common to both groups. 

Dynamic escapes allow conficts that are completely unknown to lexical escapes. For one thing, dynamic escapes can be used anywhere, so one function call put an escape in place, and another function may unwittingly intercept it. For example, 

\begin{listing}
(define (foo)
  (catch 'foo (* 2 (bar))))

(define (bar)
  (+ 1 (throw 'foo 5)))
\end{listing}

Independently of the extent of the escape, block limits its reference to its body whereas catch authorizes that as long as it lives. As a consequence, it is possible to use the escape bound to foo all the time that (* 2 (bar)) is being evaluated. In the preceding example, you cannot replace ``catch 'foo'' by ``block foo'' and expect the same results. An even more dangerous collision would be this:
\begin{listing}
(catch 'not-a-pair
  (better-map (lambda (x)
				(or (pair? x)
					(throw 'not-a-pair x))
				(hack-and-return-list))))
\end{listing}
Let's assume that we heard, next to the coffee-machine, that the function better-map is much better than map; and let's also suppose that we'll risk using it to rest whether the result of (hack-and-return-list) is really a list made up of pairs; and furthermore, we'll assume that we don't know the definition of better-map, which happen to be this:
\begin{listing}
(define (better-map fn lst)
  (define (loop lst1 lst2 flag)
	(if (pair? lst1)
		(if (eq? lst1 lst2)
			(throw 'not-a-pair lst)
			(cons (fn (car lst1))
				  (loop (cdr lst1)
						(if flag (cdr lst2) lst2)
						(not flag))))))
  (loop lst (cons 'ignore lst) #t))
\end{listing}
In fact, the function better-map is quite interesting because it halts even if the list in the second argument is cyclic. Yet if (hack-and-return-list) returns the infinite list \#1 = ((foo. hack) . #1#) \footnote{Here, we've used Common Lisp notation for cyclic data. We could also have built such a list by (let ((p (list (cons 'foo hack))))(set-cdr! p p ) p)} then better-map would try to escape.  If that fact is not specified in its user interface, there might be a name conflict and a collision of escapes comparable to a twenty-car pile-ip at rush hour. Here again we would change the names to limit such conflicts; doing so is simple with catch because it allows its label to be any possible Lisp value; in particular, the value could be a dotted pair that we construct ourselves. We could rewrite it more certainly this way then:
\begin{listing}
(let ((tag (list 'not-a-pair)))
  (catch tag
	(better-map (lambda (x)
				  (or (pair? x)
					  (throw tag x)))
				(foo-hack))))
\end{listing}
It is possible to simulate block by catch but there is no gain in efficiency in doing so. It suffices to convert the lexical escapes into dynamic ones, like this:
\begin{listing}
(define-syntax block
  (syntax-rules ()
	((block label . body)
	 (let ((label (list 'label)))
	   (catch label . body)))))

(define-syntax return-from
  (syntax-rules()
	((return-from label value)
	 (throw label value))))
\end{listing}

The macro block creates a unique label and binds it lexically to the variable of the same name. Doing so insures that only the return-from(s) present in its body will see that label. Of course, by doing that, we pollute the varibale space with the name label. To offset that fault, we can try to use a name that doesn't conflict or even a name created by gensym, by once we do that, we have to make arrangements for the same name to appears in both catch and throw. 

\subsection{Escapes with Indefinite Extent}
\label{sec:escap-with-indef}

As part of its massive overhaul of Lisp around 1975, Scheme proposed that continuations caught by catch or block should have an indefinite extent. This property gave them new and very interesting characteristics. Later, in an attempt to reduce the number of special forms in scheme, there was an effort to capture continuations by means of a function as well as to represent a continuation as a function, that is, as a first class cirizen. In [Lan65], Landin suggested the operator J to capture continuations, and the function call/cc in Scheme is its direct descendant. 

We'll try to explain its syntax as simply as possible at this first encounter. First of all, it involves the capture of a continuation, so we need a form that captures the continuation of its caller,k, like this:

\section{Actors in a Computation}
\section{Initializing the Interpreter}
\section{Implementing Control Forms}
\section{Comparing call/cc to catch}
\section{Programming by Continuations}
\section{Partial Continuations}
\section{Conclusions}
\section{Exercises}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
