static void *
ngx_http_circle_git_create_loc_conf(ngx_conf_t *cf){
  ngx_http_circle_git_loc_conf_t *conf =
	ngx_pcalloc(cf->pool, sizeof(ngx_http_circle_gif_loc_conf_t));

  if(conf == NULL){
	return NGX_CONF_ERROR;
  }

  conf->min_radius = NGX_CONF_UNSET_UINT;
  conf->max_radius = NGX_CONF_UNSET_UINT;
  
  return conf;
}

static char*
ngx_http_circle_gif_merge_loc_conf(ngx_conf_t *cf, void *parent,
								   void * child){
  ngx_http_circle_gif_loc_conf_t *prev = parent;
  ngx_http_circle_gif_loc_conf_t *conf = child;

  ngx_conf_merge_uint_value(conf->min_radius, prev->min_radius, 10);
  ngx_conf_merge_uint_value(conf->max_radius, prev->max_radius, 20);

  if(conf->min_radius < 1){
	ngx_conf_log_error(ngx_log_emerg, cf, 0,
					   "min radius must be equal or more than 1");
	return NGX_CONF_ERROR;
  }

  if(conf->max_radius < conf->min_radius){
	ngx_conf_log_error(ngx_log_emerg, cf, 0,
					   "max radius must be equal or more than min_radius");
	return NGX_CONF_ERROR;
  }
}


