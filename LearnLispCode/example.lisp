(cons 1 2)
(cons "hello" 2)

'(1 2 3 4 5)
(cons 1 (cons 2  (cons 3 (cons 4 (cons 5 nil)))))

(ql:quickload :draw-cons-tree)


(defvar f 10)
(defparameter bar 20)
(defconstant +baz+ 30)

