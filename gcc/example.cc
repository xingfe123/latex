
static ngx_int_t
ngx_http_proxy_handler(ngx_http_request_t *req){
  ngx_http_proxy_loc_conf_t *p1cf
	= ngx_http_get_module_loc_conf(req, ngx_http_proxy_module);
  assert(p1cf != NULL);

  ngx_http_upstream_t *u
	= ngx_pcalloc(req->pool, sizeof(ngx_http_upstream_t));
  if(u == NULL){
	return NGX_HTTP_internal_server_error;
  }

  u->peer.log = req->connection->log;
  u->peer.log_error = NGX_error_err;

  u->output.tag = (ngx_buf_tag_t) &ngx_http_proxy_module;
  u->conf = &p1cf->upstream;

  // attach the callback functions;

  u->create_request = ngx_http_proxy_create_request;
  u->reinit_request = ngx_http_proxy_reinit_request;
  u->process_header = ngx_http_proxy_process_status_line;
  u->abort_request = ngx_http_proxy_abort_request;
  u->finalize_request = ngx_http_proxy_finalize_request;

  r->upstream = u;

  rc = ngx_http_read_client_request_body(req, ngx_http_upstream_init);
  if(rc >= NGX_HTTP_special_response){
	return rc;
  }

  return NGX_DONE;
}

static ngx_int_t
ngx_http_character_server_create_request(ngx_http_request_t *req){
  // make a buffer and chain
  ngx_buf_t *b = ngx_create_temp_buf(req->pool, sizeof("a")-1);
  if(b == NULL){
	return NGX_ERROR;
  }

  ngx_chain_t* c1 = ngx_alloc_chain_link(req->pool);
  if(c1 == NULL){
	return NGX_ERROR;
  }

  // hook the buffer to the chain .
  c1->buf = b;

  // chain to the upstream
  r->upstream->request_bufs = c1;

  // now write to the buffer
  b->pos = "a";
  b->last = b->pos + sizeof("a") -1;

  return NGX_OK;
}
