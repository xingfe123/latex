#coding: utf-8
print(__doc__)
import numpy as np
from matplotlib.font_manager import FontProperties

# Create a the dataset
rng = np.random.RandomState(1)
X = np.linspace(0, 6, 100)[:, np.newaxis]
y = np.sin(X).ravel() + np.sin(6 * X).ravel() + rng.normal(0, 0.1, X.shape[0])

# Fit regression model
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor

clf_1 = DecisionTreeRegressor(max_depth=4)

clf_2 = AdaBoostRegressor(DecisionTreeRegressor(max_depth=5),
                          n_estimators=300, random_state=rng)

clf_1.fit(X, y)
clf_2.fit(X, y)

# Predict
y_1 = clf_1.predict(X)
y_2 = clf_2.predict(X)

# Plot the results
import pylab as pl
font = FontProperties(fname=r"/usr/share/fonts/win/SIMKAI.ttf", size=14) 
pl.figure()#fontproperties=font)
pl.scatter(X, y, c="k", label= u"traning samples ")#fontproperties=font)
pl.plot(X, y_1, c="g", label=u"decision tree", linewidth=2)
pl.plot(X, y_2, c="r", label=u"adaboosting 300个decision", linewidth=2)
pl.xlabel(u"数据",fontproperties=font)
pl.ylabel(u"目标函数",fontproperties=font)
pl.title("测试 y=sin(x)+sin(6*x)",fontproperties=font)
pl.legend()
pl.show()
#pl.save('1.png')
