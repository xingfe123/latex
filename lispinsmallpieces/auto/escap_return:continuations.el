(TeX-add-style-hook
 "escap_return:continuations"
 (lambda ()
   (LaTeX-add-labels
    "sec:escapes-with-dynamic"
    "sec:comp-catch-block"
    "sec:escap-with-indef"))
 :latex)

