(TeX-add-style-hook
 "denotationalsemantics"
 (lambda ()
   (LaTeX-add-labels
    "eq:1"
    "eq:2"
    "eq:3"
    "eq:4"
    "eq:5"
    "eq:6"
    "eq:7"
    "eq:8"
    "eq:9"
    "eq:10"
    "eq:11"
    "eq:12"))
 :latex)

