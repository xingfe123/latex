(TeX-add-style-hook
 "thebasicsofinterpretation"
 (lambda ()
   (LaTeX-add-labels
    "fig:exampleplot")
   (LaTeX-add-environments
    '("problem" LaTeX-env-args ["argument"] 1)))
 :latex)

